#!/bin/bash

DOCKER_COMPOSE=$(which docker-compose)

if [ -z "$DOCKER_COMPOSE" ]
then
    echo "Error: docker-compose not found"
    exit 1
fi

$DOCKER_COMPOSE up --build -d