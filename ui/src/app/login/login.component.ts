import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
  ) {
  }

  submitForm(): void {
    if (!this.formGroup.valid) {
      return;
    }

    const data = {
      email: this.formGroup.controls.email.value,
      password: this.formGroup.controls.password.value,
      remember: this.formGroup.controls.remember.value,
    };

    this.httpClient
      .post<any>('/api/user/login', data)
      .subscribe(res => {
        localStorage.setItem('token', res.session_id);
        location.pathname = '/';
      });
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      remember: [true],
    });
  }
}
