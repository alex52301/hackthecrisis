import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ZorroModule} from '../../zorro.module';

import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {UserProductsComponent} from './user-products/user-products.component';
import {UserTransactionsComponent} from './user-transactions/user-transactions.component';


@NgModule({
  declarations: [ProfileComponent, UserTransactionsComponent, UserProductsComponent, UserDetailsComponent],
  imports: [
    ZorroModule,
    FormsModule,
    CommonModule,
    ProfileRoutingModule,
  ],
})
export class ProfileModule {
}
