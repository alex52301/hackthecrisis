import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Product} from '../../../models/product';

@Component({
  selector: 'app-products',
  templateUrl: './user-products.component.html',
  styleUrls: ['./user-products.component.scss'],
})
export class UserProductsComponent implements OnInit {
  products: Observable<Product[]> = of([
    {uuid: '1', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, editModal: false},
    {uuid: '2', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, editModal: false},
    {uuid: '3', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, editModal: false},
  ]);

  addModal = false;
  newProduct: Product = new Product();

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  get token() {
    return localStorage.getItem('token');
  }


  ngOnInit(): void {
    // TODO load products

    this._loadProducts();
  }

  edit(product) {
    console.log(product);
    product.editModal = false;
    // TODO update products
  }

  delete(product) {
    // const index = this.products.indexOf(product);
    // this.products.splice(index, 1);

    // TODO delete product
  }

  add() {
    const headers = {
      authorization: this.token,
    };

    this.newProduct.startsOn = new Date(this.newProduct.startsOn).getTime();
    this.newProduct.endsOn = new Date(this.newProduct.endsOn).getTime();

    this.httpClient.post('api/product/create', this.newProduct, {headers})
      .subscribe(res => {

        console.log(res);
        this.newProduct = new Product();
        this.addModal = false;
        this._loadProducts();
      });
  }

  private _loadProducts() {
    const headers = {
      authorization: this.token,
    };

    this.products = this.httpClient.get<Product[]>('api/user/products', {headers})
      .pipe(
        tap(products => {
          console.log(products);
        }),
      );
  }
}
