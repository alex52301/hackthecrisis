import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Transaction} from '../../../models/transaction';

@Component({
  selector: 'app-transactions',
  templateUrl: './user-transactions.component.html',
  styleUrls: ['./user-transactions.component.scss'],
})
export class UserTransactionsComponent implements OnInit {
  transactions: Observable<Transaction[]> = of([]);

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  get token() {
    return localStorage.getItem('token');
  }

  ngOnInit(): void {
    const headers = {
      authorization: this.token,
    };

    this.transactions = this.httpClient
      .get<Transaction[]>('api/transaction', {headers})
      .pipe(
        tap(res => {
          console.log(res);
        }),
      );
  }
}
