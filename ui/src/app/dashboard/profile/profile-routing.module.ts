import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ProfileComponent} from './profile.component';
import {UserDetailsComponent} from './user-details/user-details.component';
import {UserProductsComponent} from './user-products/user-products.component';
import {UserTransactionsComponent} from './user-transactions/user-transactions.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [
      {path: '', component: UserDetailsComponent},
      {path: 'products', component: UserProductsComponent},
      {path: 'transactions', component: UserTransactionsComponent},
    ],
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {
}
