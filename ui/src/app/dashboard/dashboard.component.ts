import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor() {
  }

  get token() {
    return localStorage.getItem('token');
  }

  ngOnInit(): void {
  }

  logout() {
    localStorage.removeItem('token');
  }
}
