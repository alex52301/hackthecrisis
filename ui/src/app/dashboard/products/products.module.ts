import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ZorroModule} from '../../zorro.module';

import {ProductsRoutingModule} from './products-routing.module';
import {ProductsComponent} from './products.component';


@NgModule({
  declarations: [ProductsComponent],
  imports: [
    ZorroModule,
    FormsModule,
    CommonModule,
    ProductsRoutingModule,
  ],
})
export class ProductsModule {
}
