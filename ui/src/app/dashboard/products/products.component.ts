import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Product} from '../../models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

  products: Observable<Product[]> = of([
    {uuid: '1', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, buyModal: false},
    {uuid: '2', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, buyModal: false},
    {uuid: '3', name: 'Romantic dinner', price: 100, percentageDiscount: .3, quantity: 1, buyModal: false},
  ]);

  quantity = 1;
  buyProduct: Product;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  get token() {
    return localStorage.getItem('token');
  }

  ngOnInit(): void {
    const headers = {
      authorization: this.token,
    };

    this.products = this.httpClient.get<Product[]>('api/product', {headers})
      .pipe(
        tap(products => {
          console.log(products);
        }),
      );
  }

  buy() {
    console.log(this.buyProduct);

    const body = {
      quantity: this.quantity,
      product_uuid: this.buyProduct.uuid,
    };

    const headers = {
      authorization: this.token,
    };

    this.httpClient.post('api/transaction/create', body, {headers})
      .subscribe(res => {
        console.log(res);

        this.quantity = 1;
        this.buyProduct = undefined;
        location.pathname = '/profile/transactions';
      });
  }
}
