import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {ZorroModule} from '../zorro.module';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';


@NgModule({
  declarations: [DashboardComponent],
  imports: [
    ZorroModule,
    CommonModule,
    DashboardRoutingModule,
  ],
})
export class DashboardModule {
}
