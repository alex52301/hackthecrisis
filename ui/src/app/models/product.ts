export enum ProductStatus {
  Active = 'active',
  Deactivated = 'deactivated'
}

export enum ProductType {
  Service = 'service',
  Item = 'item'
}


export class Product {
  uuid?: string;

  name?: string;
  description?: string;
  image?: string;
  status?: ProductStatus = ProductStatus.Active;
  type?: ProductType = ProductType.Item;
  startsOn?: number;
  endsOn?: number;
  percentageDiscount?: number;
  amountDiscount?: number;
  price?: number;
  quantity?: number;

  editModal?: boolean;
  buyModal?: boolean;
}
