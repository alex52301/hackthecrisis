export class Transaction {
  uuid: string;
  ethereumTransactionId: string;
  description: string;
  percentageDiscount: number;
  quantity: number;
  price: number;
}
