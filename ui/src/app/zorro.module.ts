import {NgModule} from '@angular/core';
import {
  NzButtonModule,
  NzCardModule,
  NzCheckboxModule,
  NzFormModule,
  NzGridModule,
  NzIconModule,
  NzInputModule,
  NzInputNumberModule,
  NzLayoutModule,
  NzMenuModule,
  NzModalModule,
} from 'ng-zorro-antd';

const modules = [
  NzFormModule,
  NzCardModule,
  NzMenuModule,
  NzIconModule,
  NzGridModule,
  NzInputModule,
  NzModalModule,
  NzLayoutModule,
  NzButtonModule,
  NzCheckboxModule,
  NzInputNumberModule,
];

@NgModule({
  declarations: [],
  imports: modules,
  exports: modules,
})
export class ZorroModule {
}
