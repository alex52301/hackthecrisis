import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {ZorroModule} from '../zorro.module';
import {SignupRoutingModule} from './signup-routing.module';
import {SignupComponent} from './signup.component';


@NgModule({
  declarations: [SignupComponent],
  imports: [
    ZorroModule,
    CommonModule,
    HttpClientModule,
    SignupRoutingModule,
    ReactiveFormsModule,
  ],
})
export class SignupModule {
}
