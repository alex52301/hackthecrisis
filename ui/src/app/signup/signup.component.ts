import {HttpClient} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
  ) {
  }

  submitForm(): void {
    if (!this.formGroup.valid) {
      return;
    }

    const data = {
      email: this.formGroup.controls.email.value,
      password: this.formGroup.controls.password.value,
      fname: this.formGroup.controls.fname.value,
      lname: this.formGroup.controls.lname.value,
    };

    this.httpClient
      .post('/api/user/sign-up', data)
      .subscribe(res => {
        location.pathname = '/login';
      });
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
      fname: [null, [Validators.required]],
      lname: [null, [Validators.required]],
    });
  }
}

