import * as express from 'express';
import { check, param } from 'express-validator';
import TransactionController from '../../controllers/TransactionController';
import {ApiHelper} from '../../helpers/ApiHelper';
import { UserSchemaInterface, UserType } from '../../models/User';
import { CustomError } from '../../helpers/CustomError';
import { ProductSchemaInterface } from '../../models/Product';
import ProductController from '../../controllers/ProductController';
import UserController from '../../controllers/UserController';
import { Schema } from 'mongoose';

const router = express.Router();

// router.post('/issue-transaction', [
//     check('privatekey').not().isEmpty().isEmail(),
//     check('publickey').not().isEmpty(),
//     check('amount').not().isEmpty(),
//     check('details').not().isEmpty(),
//
// ], async (req, res) => {
//
//     const bodyParams = {
//         description: req.body.description as string,
//         privatekey: req.body.privatekey as string,
//         publickey: req.body.publickey as string,
//         amount: req.body.amount as number,
//     };
//
//     ApiHelper.requestWrapper(req, res, async () => {
//
//         const transactionId = await TransactionController.issueTransaction(bodyParams.privatekey, bodyParams.publickey, bodyParams.amount, bodyParams.description);
//
//         return {
//             transaction_id: transactionId,
//         };
//
//     });
// });

router.post('/create', [
    check('product_uuid').not().isEmpty(),
    check('quantity').not().isEmpty().isNumeric(),

], async (req, res) => {

    const bodyParams = {
        product_uuid: req.body.product_uuid as string,
        quantity: req.body.quantity as number,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        if (!user.verified) {
            throw new CustomError('token', 'You need to be verified to create a product');
        }

        if (bodyParams.quantity <= 0) {
            throw new CustomError('quantity', 'Quantity cannot be less than 1');
        }

        const product = await ProductController.getProductByUuid(bodyParams.product_uuid);

        const quantityLeft = product.quantity - bodyParams.quantity;

        if (quantityLeft < 0) {
            throw new CustomError('quantity', 'Insufficient stock');
        }

        if (product.endsOn.getTime() < new Date().getTime()) {
            throw new CustomError('product_uuid', 'This product has ended');
        }

        if (product.startsOn.getTime() > new Date().getTime()) {
            throw new CustomError('product_uuid', 'This product has not started yet');
        }

        const transaction = await TransactionController.createTransaction(user, product, bodyParams.quantity);

        return TransactionController.getTransactionResponseStructure(transaction, transaction.seller as unknown as UserSchemaInterface, user);

    });
});

router.post('/exchange', [
    check('buyer_product_uuid').not().isEmpty(),
    check('buyer_product_quantity').not().isEmpty().isNumeric(),
    check('product_uuid').not().isEmpty(),
    check('quantity').not().isEmpty().isNumeric(),

], async (req, res) => {

    const bodyParams = {
        product_uuid: req.body.product_uuid as string,
        buyer_product_uuid: req.body.buyer_product_uuid as string,
        quantity: req.body.quantity as number,
        buyer_product_quantity: req.body.buyer_product_quantity as number,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        if (!user.verified) {
            throw new CustomError('token', 'You need to be verified to create a product');
        }

        if (bodyParams.quantity <= 0) {
            throw new CustomError('quantity', 'Quantity cannot be less than 1');
        }

        const productToBeExchanged = await ProductController.getProductByUuid(bodyParams.buyer_product_uuid);

        if (productToBeExchanged == null) {
            throw new CustomError('buyer_product_uuid', 'Product does not exist');
        }

        const productToBeExchangedQuantityLeft = productToBeExchanged.quantity - bodyParams.buyer_product_quantity;

        if (productToBeExchangedQuantityLeft < 0) {
            throw new CustomError('buyer_product_quantity', 'Your product has insufficient stock');
        }

        if (productToBeExchanged.endsOn.getTime() < new Date().getTime()) {
            throw new CustomError('buyer_product_uuid', 'Your product has ended');
        }

        if (productToBeExchanged.startsOn.getTime() > new Date().getTime()) {
            throw new CustomError('buyer_product_uuid', 'Your product has not started yet');
        }

        const product = await ProductController.getProductByUuid(bodyParams.product_uuid);

        const quantityLeft = product.quantity - bodyParams.quantity;

        if (quantityLeft < 0) {
            throw new CustomError('quantity', 'Insufficient stock');
        }

        if (product.endsOn.getTime() < new Date().getTime()) {
            throw new CustomError('product_uuid', 'This product has ended');
        }

        if (product.startsOn.getTime() > new Date().getTime()) {
            throw new CustomError('product_uuid', 'This product has not started yet');
        }

        const transaction = await TransactionController.createExchangeTransaction(user, product, bodyParams.quantity, productToBeExchanged, bodyParams.buyer_product_quantity);

        return TransactionController.getTransactionResponseStructure(transaction, transaction.seller as unknown as UserSchemaInterface, user);

    });
});

router.post('/execute-exchange', [
    check('uuid').not().isEmpty(),
    check('accepted').not().isEmpty().isBoolean(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.body.uuid as string,
        accepted: req.body.accepted as boolean,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        if (!user.verified) {
            throw new CustomError('token', 'You need to be verified to create a product');
        }

        // let transaction = await TransactionController.getTransactionByUuidWithoutPopulate(bodyParams.uuid);


        // if ((transaction.seller as  Schema.Types.ObjectId).toString() !== user._id.toString()) {
        //     throw new CustomError('token', 'No access');
        // }

        let transaction = await TransactionController.executeExchangeTransaction(bodyParams.uuid, bodyParams.accepted);

        return TransactionController.getTransactionResponseStructure(transaction, user, transaction.buyer as unknown as UserSchemaInterface);

    });
});

router.get('/', async (req, res) => {

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (user == null) {
            throw new CustomError('token', 'No access');
        }

        const transactions = await TransactionController.getUserTransactions(user);

        return transactions.map((transaction) => {
            return TransactionController.getTransactionResponseStructure(transaction, transaction.seller as unknown as UserSchemaInterface, transaction.buyer as unknown as UserSchemaInterface);
        })

    });
});

router.get('/:uuid', [
    param('uuid').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (user == null) {
            throw new CustomError('token', 'No access');
        }

        const transaction = await TransactionController.getTransactionByUuid(bodyParams.uuid);

        if (transaction == null) {
            throw new CustomError('uuid', 'Transaction not exists');
        }

        if (user.uuid !== (transaction.seller  as unknown as UserSchemaInterface).uuid && user.uuid !== (transaction.buyer as unknown as UserSchemaInterface).uuid) {
            throw new CustomError('uuid', 'No access');
        }

        return TransactionController.getTransactionResponseStructure(transaction, transaction.seller as unknown as UserSchemaInterface, transaction.buyer as unknown as UserSchemaInterface);

    });
});

// Export the router
export default router;
