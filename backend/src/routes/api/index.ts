import * as express from 'express';
import user from './user';
import product from './product';
import transaction from './transaction';
import { ApiHelper } from '../../helpers/ApiHelper';

const router = express.Router();

router.use(async (req, res, next) => {

    // this is for authentication
    const exists = await ApiHelper.tokenExists(req.headers.authorization);

    if (exists) {
        req.user = await ApiHelper.getUserFromToken(req.headers.authorization);
    }

    next();

});

router.use('/user', user);
router.use('/product', product);
router.use('/transaction', transaction);

export default router;