import * as express from 'express';
import { ApiHelper } from '../../helpers/ApiHelper';
import UserController from '../../controllers/UserController';
import { check, param } from 'express-validator';
import { CustomError } from '../../helpers/CustomError';
import { UserInterface, UserSchemaInterface, UserType } from '../../models/User';
import ProductController from '../../controllers/ProductController';
import uuid = require('uuid');

const router = express.Router();

router.post('/sign-up', [
    check('email').not().isEmpty().isEmail(),
    check('password').not().isEmpty(),
    check('fname').not().isEmpty(),
    check('lname').not().isEmpty()
], async (req, res) => {

    const bodyParams = {
        email: req.body.email as string,
        password: req.body.password as string,
        fname: req.body.fname as string,
        lname: req.body.lname as string,
        type: req.body.type as UserType,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = await UserController.getUserByEmail(bodyParams.email);

        if (user != null) {
            throw new CustomError('email', 'Email already exists');
        }

        const hashedPass = await ApiHelper.hashPassword(bodyParams.password);

        const newUser: UserInterface = {
            firstName: bodyParams.fname,
            lastName: bodyParams.lname,
            email: bodyParams.email,
            uuid: uuid.v4(),
            image: '',
            password: hashedPass,
            verified: true,
            type: UserType.Individual,
        };

        const createdUser = await UserController.createUser(newUser);

        const sessionToken = await ApiHelper.loginUser(createdUser.uuid);

        return {
            session_id: sessionToken,
        };

    });
});

router.post('/login', [
    check('email').not().isEmpty().isEmail(),
    check('password').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        email: req.body.email as string,
        password: req.body.password as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = await UserController.getUserByEmail(bodyParams.email);

        if (user == null) {
            throw new CustomError('email', 'Email does not exists');
        }

        const isPassCorrect = await ApiHelper.comparePassword(bodyParams.password, user.password);

        if (!isPassCorrect) {
            throw new CustomError('password', 'username or password incorrect');
        }

        const sessionToken = await ApiHelper.loginUser(user.uuid);

        return {
            session_id: sessionToken,
        };

    });
});

router.get('/products', async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const sessionUser = req.user as UserSchemaInterface;

        if (!sessionUser) {
            throw new CustomError('token', 'No access');
        }

        const userProducts = await ProductController.getUserProducts(sessionUser);

        return userProducts.map( (product) => {
            return ProductController.getProductResponseStructure(product);
        });

    });
});

router.get('/:uuid', [
    param('uuid').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = await UserController.getUserByUuid(bodyParams.uuid);

        if (user == null) {
            throw new CustomError('uuid', 'User does not exists');
        }
        const userResponse = UserController.getUserResponseStructure(user);

        return userResponse;

    });
});

router.delete('/:uuid', [
    param('uuid').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const sessionUser = req.user as UserSchemaInterface;

        if (!sessionUser) {
            throw new CustomError('token', 'No access');
        }

        if (!sessionUser) {
            throw new CustomError('uuid', 'You can delete only your user');
        }

        const user = await UserController.getUserByUuid(bodyParams.uuid);

        if (user == null) {
            throw new CustomError('uuid', 'User does not exists');
        }

        await UserController.deleteUserByUuid(bodyParams.uuid);

        return {};

    });
});

// Export the router
export default router;
