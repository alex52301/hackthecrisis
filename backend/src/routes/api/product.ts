import * as express from 'express';
import { ApiHelper } from '../../helpers/ApiHelper';
import UserController from '../../controllers/UserController';
import { check, param } from 'express-validator';
import { CustomError } from '../../helpers/CustomError';
import { UserInterface, UserSchemaEnum, UserSchemaInterface, UserType } from '../../models/User';
import uuid = require('uuid');
import ProductController from '../../controllers/ProductController';
import { Schema } from "mongoose";
import { ProductInterface, ProductStatus, ProductType } from '../../models/Product';

const router = express.Router();

router.get('/', async (req, res) => {

    ApiHelper.requestWrapper(req, res, async () => {

        const products = await ProductController.getAllProducts();

        return products.map( (product) => {
            return ProductController.getProductResponseStructure(product)
        });

    });
});

router.get('/:uuid', [
    param('uuid').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        const product = await ProductController.getProductByUuid(bodyParams.uuid);

        if (product == null) {
            throw new CustomError('uuid', 'Item does not exist');
        }

        if (user.uuid !== (product.addedBy as unknown as UserSchemaInterface).uuid) {
            throw new CustomError('uuid', 'No access');
        }

        const productResponse = ProductController.getProductResponseStructure(product);

        return productResponse;

    });
});

router.delete('/:uuid', [
    param('uuid').not().isEmpty(),
], async (req, res) => {

    const bodyParams = {
        uuid: req.params.uuid as string,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const product = await ProductController.getProductByUuid(bodyParams.uuid);
        const user = req.user;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        if (product == null) {
            throw new CustomError('uuid', 'Item does not exist');
        }

        if (user.uuid !== (product.addedBy as unknown as UserSchemaInterface).uuid) {
            throw new CustomError('uuid', 'No access');
        }

        await ProductController.deleteProductByUuid(bodyParams.uuid);

        return {};

    });
});

router.post('/create', [
    check('name').not().isEmpty(),
    check('price').not().isEmpty(),
    check('image'),
    check('quantity').not().isEmpty().isNumeric(),
    check('status').isIn([ProductStatus.Active, ProductStatus.Deactivated]),
    check('type').isIn([ProductType.Item, ProductType.Service]),
    check('startsOn'),
    check('endsOn'),
    check('percentageDiscount'),
    check('amountDiscount'),
    check('description'),
], async (req, res) => {

    const bodyParams = {
        name: req.body.name as string,
        image: (req.body.image) ? req.body.image as string : '',
        status: req.body.status as ProductStatus,
        type: req.body.type as ProductType,
        startsOn: req.body.startsOn as number,
        endsOn: req.body.endsOn as number,
        percentageDiscount: req.body.percentageDiscount as number,
        amountDiscount: req.body.amountDiscount as number,
        price: req.body.price as number,
        quantity: req.body.quantity as number,
        description: req.body.description as string | undefined,
    };

    ApiHelper.requestWrapper(req, res, async () => {

        const user = req.user as UserSchemaInterface;

        if (!user) {
            throw new CustomError('token', 'No access');
        }

        if (!user.verified) {
            throw new CustomError('token', 'You need to be verified to create a product');
        }

        if (bodyParams.percentageDiscount && bodyParams.amountDiscount) {
            throw new CustomError('percentageDiscount', 'Only one type of a discount can be used');
        }

        const newProduct: ProductInterface = {
            uuid: uuid.v4(),
            name: bodyParams.name,
            image: bodyParams.image,
            startsOn: bodyParams.startsOn ? new Date(Number(bodyParams.startsOn)) : null,
            endsOn: bodyParams.endsOn ? new Date(Number(bodyParams.endsOn)) : null,
            status: bodyParams.status,
            type: bodyParams.type,
            amountDiscount: bodyParams.amountDiscount,
            percentageDiscount: bodyParams.percentageDiscount,
            price: bodyParams.price,
            addedBy: user._id,
            quantity: bodyParams.quantity,
            description: (bodyParams.description) ? bodyParams.description : '',
        };

        const createdProduct = await ProductController.createProduct(newProduct);

        return ProductController.getProductResponseStructure(createdProduct);

    });
});

// Export the router
export default router;
