import { UserSchemaInterface } from '../models/User';

declare global {
    namespace Express {
        export interface Request {
            user?: UserSchemaInterface;
        }
    }
}