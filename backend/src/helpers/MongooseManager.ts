import * as mongoose from "mongoose";
import config from "../config";
import {Logger} from "./LoggerManager";

export default class MongooseManager {

    private static instance: MongooseManager;
    private url: string;

    private constructor() {
        this.url = config.mongo.host + ':' + config.mongo.port;
    }

    public static getInstance() {
        if (!MongooseManager.instance) {
            MongooseManager.instance = new MongooseManager();
        }
        return MongooseManager.instance;
    }

    public async init(): Promise<void> {

        try {
            await mongoose.connect(this.url, {
                useNewUrlParser: true,
                poolSize: 10,
                reconnectTries: 30,
                dbName: config.mongo.db,
                user: config.mongo.user,
                pass: config.mongo.pass
            });

            Logger.info('Server connected with Mongo db');
        } catch (e) {
            Logger.error('Server couldn\'t connect with Mongo db');
            process.exit(-1);
        }

    }

    public async closeConnection(): Promise<void> {
        await mongoose.disconnect();
    }
}