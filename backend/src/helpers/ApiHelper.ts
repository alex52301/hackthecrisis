import * as express from 'express';
import { validationResult } from 'express-validator';
import { compare, genSalt, hash } from 'bcrypt';
import { randomBytes } from 'crypto';
import { Logger } from './LoggerManager';
import config from '../config';
import { RedisHelper } from './RedisHelper';
import UserController from '../controllers/UserController';
import User, { UserSchemaInterface } from '../models/User';

interface ApiResultObjectError {
    code: string;
    number: number;
    message: string;
}

interface ApiResultObject {
    [key: string]: any;
    error?: ApiResultObjectError;
}

export class ApiHelper {
    public static async requestWrapper(req: express.Request, res: express.Response, handler: () => Promise<ApiResultObject>) {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            Logger.error('ApiHelper.requestWrapper() Express Validation Errors', req, errors.array());
            return res.status(422).json({ error: errors.array() });
        }


        try {
            const handlerResult = await handler();

            res.send(handlerResult);
        } catch (err) {
            console.error(err);
            Logger.error('ApiHelper.requestWrapper() Error: ', err);

            if (err.fieldError != undefined) {
                return res.status(400).send({ field: err.fieldError, msg: err.message });
            }
            return res.status(500).send({ error: 'Internal Server Error' });
        }
    }

    public static async hashPassword(password: string): Promise<string> {
        const saltRounds = 10;

        return new Promise<string>((resolve, reject) => {
            genSalt(saltRounds, (err, salt) => {
                if (err) {
                    reject(err);
                    return;
                }

                hash(password, salt, (hashError, encrypted) => {

                    if(hashError) {
                        return reject(hashError)
                    }

                    return resolve(encrypted);

                })

            });
        });

    }

    public static async comparePassword(password, hashDb): Promise<boolean> {
        // Load hash from your password DB.
        const isSame = await compare(password, hashDb);
        return isSame;
    }

    public static async generateToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            randomBytes(48, (err, buffer) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(buffer.toString('hex'));
                }
            });
        });
    }

    public static async loginUser(userId: string): Promise<string> {
        const sessionToken = await ApiHelper.generateToken();

        await RedisHelper.setAsync(this.buildRedisApiTokenKey(sessionToken), userId, config.session.duration);

        return sessionToken;
    }

    private static buildRedisApiTokenKey(token: string): string {

        const rediApiKey = config.session.prefix + token;

        return rediApiKey;
    }

    public static async tokenExists(sessionToken: string): Promise<boolean> {

        const tokenValue = await RedisHelper.getAsync(this.buildRedisApiTokenKey(sessionToken));

        if (tokenValue) {
            return true;
        }

        return false;
    }

    public static async getUserFromToken(sessionToken: string): Promise<UserSchemaInterface | null> {

        const tokenValue = await RedisHelper.getAsync(this.buildRedisApiTokenKey(sessionToken));

        const user = await UserController.getUserByUuid(tokenValue);

        return user;
    }

}
