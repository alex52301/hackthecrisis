import { createClient, RedisClient, RetryStrategy } from 'redis';
import config from '../config';
import { Logger } from './LoggerManager';

export class RedisHelper {

    private static redisClient: RedisClient | null = null;

    public static getRedisClient(): RedisClient {

        if (RedisHelper.redisClient === null) {
            RedisHelper.redisClient = this.createRedisClient();
        }

        return RedisHelper.redisClient;

    }

    public static createRedisClient(): RedisClient {
        const redisClientCreation = createClient( {
           port: Number(config.redis.port),
           host: config.redis.host,
            retry_strategy: ( (options) => {

                if (options.attempt > config.redis.connection_attempt || options.total_retry_time > config.redis.total_retry_time) {
                    Logger.error('Could not connect to Redis', options.attempt, options.total_retry_time);
                    throw new Error('Could not connect to Redis');
                }

                return Math.min(options.attempt * 100, 3000);

            }) as RetryStrategy

        });

        redisClientCreation.on( 'connect', () => {
            Logger.info('Redis on connect')
        });

        redisClientCreation.on( 'error', (error) => {
            Logger.error('Redis on error', error)
        });

        return redisClientCreation;
    }

    public static getAsync(key: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.getRedisClient().get(key, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    public static setAsync(key: string, value: string, durationSec?: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (typeof durationSec !== 'undefined') {
                this.getRedisClient().set(key, value, 'EX', durationSec, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } else {
                this.getRedisClient().set(key, value, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }
        });
    }


}