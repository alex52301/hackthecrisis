import * as createError from 'http-errors';
import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as logger from 'morgan';
import * as path from 'path';
import indexRouter from './routes/api';
import { ApiHelper } from './helpers/ApiHelper';
import * as http from "http";
import { Logger } from './helpers/LoggerManager';
import * as winston from 'winston';
import MongooseManager from './helpers/MongooseManager';
import { RedisHelper } from './helpers/RedisHelper';
const app = express();

app.use(function (req, res, next) {
    res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.setHeader('Expires', '-1');
    res.setHeader('Pragma', 'no-cache');
    res.setHeader('Content-Type', 'application/json');
    next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Define request response in root URL (/)
app.get('/', function (req, res) {
    res.send('This is the backend')
});

app.use('/api', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('Endpoint does not exists');
});

const port = process.env.PORT || '8081';

app.set('port', port);

const server = http.createServer(app);

/**
 * Create HTTP server.
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
server.on('close', () => {console.log('server closed')});

async function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

    RedisHelper.getRedisClient();

    await MongooseManager.getInstance().init();

    Logger.info('Webserver started on: ', bind);
}

/**
 * Event listener for HTTP server “error” event.
 */

function onError(error: Error) {

    const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    winston.error(bind, error);
    process.exit(1);
}

process.on('SIGINT', async function () {
    console.log("Caught interrupt signal");

    await MongooseManager.getInstance().closeConnection();

});
