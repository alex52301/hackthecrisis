import User, { UserInterface, UserResponseStructure, UserSchemaEnum, UserSchemaInterface } from '../models/User';
import { Logger } from '../helpers/LoggerManager';
import { Schema } from "mongoose";

export default class UserController {

    public static async createUser(user: UserInterface): Promise<UserSchemaInterface> {

        let newUser: UserSchemaInterface = new User( {...user});

        newUser = await newUser.save();

        Logger.info('UserController.createUser() successfully finished');

        return newUser;
    }

    public static async getUserByEmail(email: string): Promise<UserSchemaInterface | null> {

        const conditions = {};
        conditions[UserSchemaEnum.email] = email;

        const user = await User.findOne(conditions);

        return user;
    }

    public static async getUserByUuid(uuid: string): Promise<UserSchemaInterface | null> {

        const conditions = {};
        conditions[UserSchemaEnum.uuid] = uuid;

        const user = await User.findOne(conditions);

        return user;
    }

    public static async getUserById(userId: Schema.Types.ObjectId): Promise<UserSchemaInterface | null> {
        const user = await User.findById(userId);
        return user;
    }

    public static async deleteUserByUuid(uuid: string): Promise<void> {

        const conditions = {};
        conditions[UserSchemaEnum.uuid] = uuid;

        await User.deleteOne(conditions);
    }

    public static getUserResponseStructure(user: UserSchemaInterface): UserResponseStructure {
        return {
            firstName: user.firstName,
            lastName: user.lastName,
            uuid: user.uuid,
            email: user.email,
            image: user.image,
            type: user.type,
            verified: user.verified
        };
    }

}
