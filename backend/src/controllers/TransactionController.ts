import { Logger } from '../helpers/LoggerManager';
import { ProductSchemaInterface } from '../models/Product';
import { UserSchemaInterface } from '../models/User';
import UserController from './UserController';
import Transaction, {
    TransactionInterface,
    TransactionResponseStructure,
    TransactionSchemaEnum,
    TransactionSchemaInterface,
    TransactionStatus,
    TransactionType
} from '../models/Transaction';
import * as EthTx from 'ethereumjs-tx';
import { TxData } from 'ethereumjs-tx';
import * as EthUtil from 'ethereumjs-util';
import { Schema } from 'mongoose';
import uuid = require('uuid');
import ProductController from './ProductController';

const Web3 = require('web3');

// const EthTx=require('ethereumjs-tx')
// const EthUtil=require('ethereumjs-util')
// const util=require('util')


export default class TransactionController {

    public static async createTransaction(buyer: UserSchemaInterface, product: ProductSchemaInterface, quantity: number): Promise<TransactionSchemaInterface> {

        const privateKey = "03BBC97C66A6F86018D92067DBD23F523C8F610A84AABD40E58DF2A120DA1222";
        const sendTo = "4468088308fdfd62B7bbE043b7D1195E84e11B0e";
        const amount = 0.1;

        const seller = await UserController.getUserById(product.addedBy as Schema.Types.ObjectId);

        const description = `${buyer.firstName} bought ${product.name} from ${seller.firstName}`;

        const quantityLeft = product.quantity - quantity;

        const ethereumTransactionId = await this.issueTransaction(privateKey, sendTo, amount, description);

        const transactionObject: TransactionInterface = {
            uuid: uuid.v4(),
            ethereumTransactionId: ethereumTransactionId,
            description: description,
            amountDiscount: product.amountDiscount,
            percentageDiscount: product.percentageDiscount,
            price: product.price,
            status: TransactionStatus.finished,
            seller: seller._id,
            buyer: buyer._id,
            quantity: quantity,
            product: product._id,
            type: TransactionType.purchase,
        };

        let newTransaction: TransactionSchemaInterface = new Transaction( {...transactionObject});

        // @ts-ignore
        newTransaction = await newTransaction.save();

        newTransaction = await Transaction.findById(newTransaction._id).populate({
            path: 'seller buyer',
            model: 'User'
        }).populate({
            path: 'product',
            model: 'Product'
        });

        product.quantity = quantityLeft;

        await product.save();

        Logger.info('TransactionController.createTransaction() successfully finished');

        return newTransaction;
    }

    public static async createExchangeTransaction(buyer: UserSchemaInterface, product: ProductSchemaInterface, quantity: number, exchangeProduct: ProductSchemaInterface, exchangeProductQuantity: number): Promise<TransactionSchemaInterface> {

        const seller = await UserController.getUserById(product.addedBy as Schema.Types.ObjectId);

        const description = `${buyer.firstName} exchanged ${exchangeProduct.name} with  ${product.name} from ${seller.firstName}`;

        const quantityLeft = product.quantity - quantity;

        const transactionObject: TransactionInterface = {
            uuid: uuid.v4(),
            ethereumTransactionId: '',
            description: description,
            amountDiscount: product.amountDiscount,
            percentageDiscount: product.percentageDiscount,
            price: 0,
            status: TransactionStatus.pending,
            seller: seller._id,
            buyer: buyer._id,
            quantity: quantity,
            product: product._id,
            exchangeProduct: exchangeProduct._id,
            exchangeProductQuantity: exchangeProductQuantity,
            type: TransactionType.exchange,
        };

        let newTransaction: TransactionSchemaInterface = new Transaction( {...transactionObject});

        // @ts-ignore
        newTransaction = await newTransaction.save();

        newTransaction = await Transaction.findById(newTransaction._id).populate({
            path: 'seller buyer',
            model: 'User'
        }).populate({
            path: 'product exchangeProduct',
            model: 'Product'
        });

        product.quantity = quantityLeft;

        await product.save();

        Logger.info('TransactionController.createExchangeTransaction() successfully finished');

        return newTransaction;
    }

    public static async executeExchangeTransaction(transactionUuid: string, accepted: boolean): Promise<TransactionSchemaInterface> {

        let transaction = await this.getTransactionByUuid(transactionUuid);
        transaction.status = TransactionStatus.cancelled;

        if (accepted) {

            transaction.status = TransactionStatus.finished;
            const privateKey = "03BBC97C66A6F86018D92067DBD23F523C8F610A84AABD40E58DF2A120DA1222";
            const sendTo = "4468088308fdfd62B7bbE043b7D1195E84e11B0e";
            const amount = 0.1;

            const ethereumTransactionId = await this.issueTransaction(privateKey, sendTo, amount, transaction.description);
            transaction.ethereumTransactionId = ethereumTransactionId;

        } else {

            const exchangeProduct = await ProductController.getProductByUuid((transaction.exchangeProduct as unknown as ProductSchemaInterface).uuid);
            const product = await ProductController.getProductByUuid((transaction.product as unknown as ProductSchemaInterface).uuid);

            exchangeProduct.quantity += transaction.exchangeProductQuantity;
            product.quantity += transaction.quantity;

            await exchangeProduct.save();
            await product.save();

        }

        // @ts-ignore
        await transaction.save();

        transaction = await Transaction.findById(transaction._id).populate({
            path: 'seller buyer',
            model: 'User'
        }).populate({
            path: 'product exchangeProduct',
            model: 'Product'
        });

        Logger.info('TransactionController.createExchangeTransaction() successfully finished');

        return transaction;
    }

    public static async getTransactionByUuid(uuid: string): Promise<TransactionSchemaInterface | null> {
        const conditions = {};
        conditions[TransactionSchemaEnum.uuid] = uuid;
        const transaction = await Transaction.findOne(conditions).populate({
            path: 'seller buyer',
            model: 'User'
        }).populate({
            path: 'product',
            model: 'Product'
        });
        return transaction;
    }

    public static async getTransactionByUuidWithoutPopulate(uuid: string): Promise<TransactionSchemaInterface | null> {
        const conditions = {};
        conditions[TransactionSchemaEnum.uuid] = uuid;
        const transaction = await Transaction.findOne(conditions);
        return transaction;
    }

    public static async getUserTransactions(user: UserSchemaInterface): Promise<TransactionSchemaInterface[]> {
        const transactions = await Transaction.find({ $or: [{ seller: user._id }, { buyer: user._id }] }, null, {sort: {createdAt: -1}}).populate({
            path: 'seller buyer',
            model: 'User'
        }).populate({
            path: 'product',
            model: 'Product'
        });

        return transactions;
    }

    public static getTransactionResponseStructure(transaction: TransactionSchemaInterface, seller: UserSchemaInterface, buyer: UserSchemaInterface): TransactionResponseStructure {

        const product = (transaction.product as unknown as ProductSchemaInterface);
        const exchangeProduct = (transaction.exchangeProduct as unknown as ProductSchemaInterface);

        return {
            uuid: transaction.uuid,
            ethereumTransactionId: (transaction.ethereumTransactionId) ? transaction.ethereumTransactionId : '',
            description: transaction.description,
            amountDiscount: transaction.amountDiscount,
            percentageDiscount: transaction.percentageDiscount,
            quantity: transaction.quantity,
            price: transaction.price,
            status: transaction.status,
            seller: {
                uuid: seller.uuid,
                image: seller.image,
                name: seller.firstName,
            },
            buyer: {
                uuid: buyer.uuid,
                image: buyer.image,
                name: buyer.firstName,
            },
            product: {
                uuid: (product) ? product.uuid: '',
                image: (product) ? product.image : '',
                name: (product) ? product.name : '',
            },
            exchangeProduct: {
                uuid: (exchangeProduct) ? exchangeProduct.uuid: '',
                image: (exchangeProduct) ? exchangeProduct.image : '',
                name: (exchangeProduct) ? exchangeProduct.name : '',
            },
            exchangeQuantity: (exchangeProduct) ? transaction.exchangeProductQuantity : 0,
            type: transaction.type,
        }
    }

    public static async issueTransaction(privatekey: string, publickey: string, amount: number, description: string): Promise<string> {

        const web3 = new Web3("https://ropsten.infura.io/v3/3836a217ff0e4854ac7e3cbf9b6c3511");

        const privateKeyBuf = Buffer.from(privatekey, 'hex');

        const address = EthUtil.privateToAddress(privateKeyBuf);

        const gasPrice = await web3.eth.getGasPrice();

        let count = await web3.eth.getTransactionCount("0x" + address.toString('hex'));

        const txData: TxData = {
            nonce: web3.utils.toHex(count),
            gasPrice: web3.utils.toHex(gasPrice),
            to: "0x" + publickey,
            value: web3.utils.toHex( web3.utils.toWei('' + amount, 'ether') ),
            data: web3.utils.toHex(description),
            gasLimit: web3.utils.toHex(300000)
        };

        const txEth  =new EthTx.Transaction(txData, { chain: 3, });
        txEth.sign(privateKeyBuf);


        let transaction = await web3.eth.sendSignedTransaction( '0x' + txEth.serialize().toString('hex'));

        return transaction.transactionHash;

    }

}
