import { Logger } from '../helpers/LoggerManager';
import Product, {
    ProductInterface,
    ProductResponseStructure,
    ProductSchemaEnum,
    ProductSchemaInterface,
    ProductStatus, ProductType
} from '../models/Product';
import { UserSchemaInterface } from '../models/User';

export default class ProductController {

    public static async createProduct(item: ProductInterface): Promise<ProductSchemaInterface> {

        let newProduct: ProductSchemaInterface = new Product( {...item});

        newProduct = await (await newProduct.save()).populate({
            path: 'addedBy',
            model: 'User'
        });

        Logger.info('ProductController.createProduct() successfully finished');

        return newProduct;
    }

    public static async getProductByUuid(uuid: string): Promise<ProductSchemaInterface | null> {

        const conditions = {};
        conditions[ProductSchemaEnum.uuid] = uuid;

        const product = await Product.findOne(conditions).populate({
            path: 'addedBy',
            model: 'User'
        });

        return product;
    }

    public static async getAllProducts(): Promise<ProductSchemaInterface[]> {
        const product = await Product.find({}, null, {sort: {createdAt: -1}}).populate({
            path: 'addedBy',
            model: 'User'
        });
        return product;
    }

    public static async getUserProducts(user: UserSchemaInterface): Promise<ProductSchemaInterface[]> {

        const conditions = {};
        conditions[ProductSchemaEnum.addedBy] = user._id;

        const product = await Product.find(conditions, null, {sort: {createdAt: -1}}).populate({
            path: 'addedBy',
            model: 'User'
        });

        return product;
    }

    public static async deleteProductByUuid(uuid: string): Promise<void> {

        const conditions = {};
        conditions[ProductSchemaEnum.uuid] = uuid;

        await Product.deleteOne(conditions);
    }

    public static getProductResponseStructure(product: ProductSchemaInterface): ProductResponseStructure {

        const seller = (product.addedBy as unknown as UserSchemaInterface);

        return {
            uuid: product.uuid,
            name: product.name,
            image: product.image,
            startsOn: product.startsOn,
            endsOn: product.endsOn,
            status: product.status,
            type: product.type,
            price: product.price,
            percentageDiscount :product.percentageDiscount,
            amountDiscount: product.amountDiscount,
            quantity: product.quantity,
            description: product.description,
            seller: {
                uuid: seller.uuid,
                image: seller.image,
                name: seller.firstName + ' ' + seller.lastName,
            }
        }
    }

}
