import { Schema } from 'mongoose';
import * as mongoose from 'mongoose';

export enum TransactionStatus {
    finished = 'finished',
    pending = 'pending',
    cancelled = 'cancelled',
}

export enum TransactionType {
    purchase = 'purchase',
    exchange = 'exchange',
}

export interface TransactionResponseStructure {
    uuid: string;
    ethereumTransactionId: string;
    description: string;
    amountDiscount: number;
    percentageDiscount: number;
    quantity: number;
    price: number;
    type: TransactionType;
    status: TransactionStatus;
    seller: {
        uuid: string;
        image: string;
        name: string;
    };
    buyer: {
        uuid: string;
        image: string;
        name: string;
    };
    product: {
        uuid: string;
        image: string;
        name: string;
    };
    exchangeQuantity: number;
    exchangeProduct: {
        uuid: string;
        image: string;
        name: string;
    };
}

export interface TransactionSchemaInterface extends Document {
    _id: Schema.Types.ObjectId,
    uuid: string;
    ethereumTransactionId: string;
    description: string;
    amountDiscount: number;
    percentageDiscount: number;
    quantity: number;
    price: number;
    seller: Schema.Types.ObjectId;
    buyer: Schema.Types.ObjectId;
    status: TransactionStatus;
    product: Schema.Types.ObjectId;
    type: TransactionType;
    exchangeProduct?: Schema.Types.ObjectId;
    exchangeProductQuantity?: number;
    createdAt: string;
    updatedAt: string;
}

export interface TransactionInterface {
    _id?: Schema.Types.ObjectId,
    uuid: string;
    ethereumTransactionId: string;
    description: string;
    amountDiscount: number;
    percentageDiscount: number;
    quantity: number;
    price: number;
    seller: Schema.Types.ObjectId;
    buyer: Schema.Types.ObjectId;
    status: TransactionStatus;
    product: Schema.Types.ObjectId;
    type: TransactionType;
    exchangeProduct?: Schema.Types.ObjectId;
    exchangeProductQuantity?: number;
    createdAt?: string;
    updatedAt?: string;
}

export enum TransactionSchemaEnum {
    uuid = 'uuid',
    ethereumTransactionId= 'ethereumTransactionId',
    amountDiscount = 'amountDiscount',
    description = 'description',
    percentageDiscount = 'percentageDiscount',
    quantity = 'quantity',
    price = 'price',
    seller = 'seller',
    buyer = 'buyer',
    status = 'status',
    product = 'product',
    type = 'type',
    exchangeProduct = 'exchangeProduct',
    exchangeProductQuantity = 'exchangeProductQuantity',
    createdAt = 'createdAt',
    updatedAt = 'updatedAt',
}

const TransactionSchema: Schema = new Schema({
    uuid: { type: String, required: true , index: true},
    ethereumTransactionId: { type: String, required: false, index: true, default: '' },
    description: { type: String, required: true, index: true },
    amountDiscount: { type : Number, required: false },
    percentageDiscount: { type : Number, required: false },
    price: { type : Number, required: true },
    quantity: { type : Number, required: true },
    status: { type: String, required: true },
    type: { type: String, required: true, default: TransactionType.purchase },
    seller: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    buyer: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    product: {type: mongoose.Schema.Types.ObjectId, ref: 'Product'},
    exchangeProduct: {type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Product',},
    exchangeProductQuantity: { type : Number, required: false },
}, { timestamps: true });

// @ts-ignore
export default mongoose.model<TransactionSchemaInterface>('Transaction', TransactionSchema);