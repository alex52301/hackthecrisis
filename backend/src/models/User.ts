import * as mongoose from 'mongoose';
import { Schema, Document } from 'mongoose';

export enum UserType {
    Business = 'business',
    Individual = 'individual'
}

export interface UserResponseStructure {
    firstName: string;
    lastName: string;
    uuid: string;
    email: string;
    image: string;
    type: UserType;
    verified: boolean;
}

// @ts-ignore
export interface UserSchemaInterface extends Document {
    _id: Schema.Types.ObjectId,
    uuid: string;
    firstName: string;
    lastName: string;
    email: string;
    image: string;
    password: string;
    type: UserType;
    verified: boolean;
    createdAt: string;
    updatedAt: string;
}

export interface UserInterface {
    firstName: string;
    lastName: string;
    email: string;
    uuid: string;
    image: string;
    password: string;
    type: UserType;
    verified: boolean;
    createdAt?: string;
    updatedAt?: string;
}

export enum UserSchemaEnum {
    firstName = 'firstName',
    lastName = 'lastName',
    email = 'email',
    image = 'image',
    uuid = 'uuid',
    password = 'password',
    type = 'type',
    verified = 'verified',
    createdAt = 'createdAt',
    updatedAt = 'updatedAt',
}

const UserSchema: Schema = new Schema({
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: {
        type: String, lowercase: true, required: [true, 'can\'t be blank'], index: true,
    },
    uuid: { type: String, required: true , index: true},
    image: { type: String, required: false },
    password: { type: String, required: true },
    type: { type: String, required: true },
    verified: { type: Boolean, required: true },
}, { timestamps: true });

// @ts-ignore
export default mongoose.model<UserSchemaInterface>('User', UserSchema);
