import * as mongoose from 'mongoose';
import { Schema, Document } from 'mongoose';
import User, { UserSchemaInterface } from './User';

export enum ProductStatus {
    Active = 'active',
    Deactivated = 'deactivated'
}

export enum ProductType {
    Service = 'service',
    Item = 'item'
}

export interface ProductResponseStructure {
    uuid: string;
    name: string;
    image: string;
    startsOn: Date;
    endsOn: Date;
    status: ProductStatus;
    type: ProductType;
    amountDiscount: number;
    percentageDiscount: number;
    price: number;
    quantity: number;
    description: string;
    seller: {
        uuid: string;
        image: string;
        name: string;
    };
}

// @ts-ignore
export interface ProductSchemaInterface extends Document {
    _id: Schema.Types.ObjectId,
    uuid: string;
    name: string;
    image: string;
    startsOn: Date | null;
    endsOn: Date | null;
    status: ProductStatus;
    type: ProductType;
    amountDiscount: number;
    percentageDiscount: number;
    price: number;
    quantity: number;
    description: string;
    addedBy: Schema.Types.ObjectId | UserSchemaInterface;
    createdAt: string;
    updatedAt: string;
}

export interface ProductInterface {
    uuid: string;
    name: string;
    image: string;
    startsOn: Date | null;
    endsOn: Date | null;
    status: ProductStatus;
    type: ProductType;
    amountDiscount: number;
    percentageDiscount: number;
    price: number;
    quantity: number;
    description: string;
    addedBy: Schema.Types.ObjectId;
    createdAt?: string;
    updatedAt?: string;
}

export enum ProductSchemaEnum {
    uuid = 'uuid',
    name= 'name',
    image = 'image',
    startsOn = 'startsOn',
    endsOn = 'endsOn',
    status = 'status',
    type = 'type',
    addedBy = 'addedBy',
    amountDiscount = 'amountDiscount',
    percentageDiscount = 'percentageDiscount',
    price = 'price',
    description = 'description',
    quantity = 'quantity',
    createdAt = 'createdAt',
    updatedAt = 'updatedAt',
}

const ProductSchema: Schema = new Schema({
    uuid: { type: String, required: true , index: true},
    name: { type: String, required: true },
    image: { type: String, required: false },
    startsOn: { type : Date, required: false},
    endsOn: { type : Date, required: false },
    amountDiscount: { type : Number, required: false },
    percentageDiscount: { type : Number, required: false },
    price: { type : Number, required: true },
    quantity: { type : Number, required: true },
    status: { type: String, required: true },
    type: { type: String, required: true },
    description: { type: String, required: false, default: '' },
    addedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
}, { timestamps: true });

// @ts-ignore
export default mongoose.model<ProductSchemaInterface>('Product', ProductSchema);
