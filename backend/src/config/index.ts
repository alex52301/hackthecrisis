require('custom-env').env(process.env.NODE_ENV);

const config = {
    redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
        connection_attempt: 10,
        total_retry_time: 60 * 10 * 1000
    },
    mongo: {
        host: process.env.MONGO_DB_HOST,
        port: process.env.MONGO_DB_PORT,
        db: process.env.MONGO_DB_DATABASE,
        user: process.env.MONGO_DB_USER,
        pass: process.env.MONGO_DB_PASS,
    },
    session: {
        prefix: 'api:',
        duration: 7 * 24 *  3600 * 1000, // one week
    }
};

export default config;
